class MeetingsController < ApplicationController
  before_action :set_meeting, only: [:show, :edit, :update, :destroy, :text, :markdown, :get_meeting_attendees, :send_goals_and_agenda, :send_notes]

  # GET /meetings
  # GET /meetings.json
  def index
    
    if params[:q]
        uc_q = params[:q].upcase
        @meetings = Meeting.where("UPPER(description) like (?) OR UPPER(meeting_name) like (?)","%#{uc_q}%","%#{uc_q}%").order(date: :desc)
        render partial: "meetings/meeting_list"
      else
        @meetings = Meeting.order(date: :desc)
        @meeting = Meeting.new
        render template: "meetings/index"
    end
  end

  # GET /meetings/1
  # GET /meetings/1.json
  def show
     @users = User.all.where(company_id: session[:company_id])
  end

  def text
    text = render_to_string('meetings/text', layout: false, locals: {meeting: @meeting, goals_and_agendas_only: false})
    render plain: text, content_type: 'text/plain'
  end

  def markdown

    @markdown_rendered = @meeting.markdown_to_html(goals_and_agendas_only: false)

    render 'markdown', layout: false
  end

  # GET /meetings/new
  def new
    @meeting = Meeting.new
  end

  # GET /meetings/1/edit
  def edit
  end

  def send_goals_and_agenda
     AttendeeMailer.goals_and_agenda(@meeting,User.find(session[:current_user])).deliver_now
     render json:{ success: 1 }
  end

  def send_notes
     AttendeeMailer.notes(@meeting,User.find(session[:current_user])).deliver_now
     render json:{ success: 1 }
  end
  def get_meeting_attendees
     query = params[:q]
    respond_to do |format|
     format.json { render json: @meeting.attendees.where("email LIKE ?","#{query}%") }
     format.html { render :partial => "shared/meeting_attendees" }
    end
  end

  # POST /meetings
  # POST /meetings.json
  def create

    @meeting = Meeting.new(meeting_params)

    @meeting[:company_id] = User.find(session[:current_user]).company_id;

    respond_to do |format|
      if @meeting.save
        format.html { redirect_to @meeting, notice: 'Meeting was successfully created.' }
        format.json { render :show, status: :created, location: @meeting }
      else
        format.html { render :new }
        format.json { render json: @meeting.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /meetings/1
  # PATCH/PUT /meetings/1.json
  def update
    respond_to do |format|
      if @meeting.update(meeting_params)
        format.html { redirect_to @meeting, notice: 'Meeting was successfully updated.' }
        format.json { render :show, status: :ok, location: @meeting }
      else
        format.html { render :edit }
        format.json { render json: @meeting.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /meetings/1
  # DELETE /meetings/1.json
  def destroy
    @meeting.destroy
    respond_to do |format|
      format.html { redirect_to meetings_url, notice: 'Meeting was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_meeting
      @meeting = Meeting.find_by(id: params[:id], company_id: session[:company_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def meeting_params
      params.require(:meeting).permit(:meeting_name, :description, :start_time, :end_time, :organizer, :time_keeper, :note_taker, :actual_end_time, :company_id, :date)
    end
end
