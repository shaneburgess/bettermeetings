class ParkingLotItemsController < ApplicationController
    before_action :set_parking_lot_item, only: [:destroy]
    def create
        @parking_lot_item = ParkingLotItem.new(parking_lot_item_params)

        @parking_lot_item.save

        @meeting = Meeting.find(@parking_lot_item.meeting_id);

        render :partial => "shared/meeting_parking_lot_items"
    end

    def destroy
        @meeting = Meeting.find(@parking_lot_item.meeting_id);

        @parking_lot_item.destroy

        render :partial => "shared/meeting_parking_lot_items"
    end

    private
    def set_parking_lot_item
        @parking_lot_item = ParkingLotItem.find(params[:id])
    end

    def parking_lot_item_params
        params.permit(:text, :meeting_id)
    end

end

