class DeliverablesController < ApplicationController
    before_action :set_deliverable, only: [:destroy]

    def create

       attendee = Attendee.find_or_create_by(email: deliverable_params[:user_email], meeting_id: deliverable_params[:meeting_id])

       attendee.save

       logger.debug "Deliverable attendee id: #{attendee.id}"
  
       @deliverable = Deliverable.create!(attendee_id: attendee.id, meeting_id: deliverable_params[:meeting_id], deliverable_text: deliverable_params[:deliverable_text], due_date: deliverable_params[:due_date]);

       @deliverable.save

       @meeting = Meeting.find(@deliverable.meeting_id);

       render :partial => "shared/meeting_deliverables"

    end

    def destroy

       @meeting = Meeting.find(@deliverable.meeting_id);

       @deliverable.destroy

       render :partial => "shared/meeting_deliverables"

    end

    
    private
      def set_deliverable
        @deliverable = Deliverable.find(params[:id])
      end
      
      def deliverable_params
         params.require(:deliverable).permit(:deliverable_text, :meeting_id, :id, :user_email, :due_date)
      end

     end
