class AuthController < ApplicationController
  def login
  end
  
  def process_login
    @user = User.find_by(email: params[:email]).try(:authenticate, params[:password])
    if(@user)
      session[:current_user] = @user.id
      session[:company_id] = @user.company_id
      redirect_to params[:redirect_to] || "/"
    else
     flash[:notice] = "There was a problem logging in. Please check your username and password and try again"
     redirect_to "/login?redirect_to="+params[:redirect_to]
    end
    
  end

  def logout
          session[:current_user] = nil
          session[:company_id] = nil
          redirect_to "/"
  end
end
