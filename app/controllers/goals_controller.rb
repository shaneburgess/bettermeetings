class GoalsController < ApplicationController
    before_action :set_goal, only: [:destroy]

    def create
       @goal = Goal.new(goal_params)

       @goal.save

       @meeting = Meeting.find(@goal.meeting_id);

       render :partial => "shared/meeting_goals"

    end
    
    def destroy
       meeting_id = @goal.meeting_id
       
       @goal.destroy
       
       @meeting = Meeting.find(meeting_id)

       render :partial => "shared/meeting_goals"
    end

    private
      def set_goal
        @goal = Goal.find(params[:id])
      end
      def goal_params
         params.require(:goal).permit(:goal_text, :meeting_id, :id)
      end

     end
