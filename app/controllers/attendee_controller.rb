class AttendeeController < ApplicationController
    before_action :set_attendee, only: [:destroy]

  def create
     @new_attendee_ids = Array.new;
     
     attendee_params[:email].split(",").each do |email|
         email.delete(' ')
         attendee = Attendee.find_or_create_by(email: email, meeting_id: attendee_params[:meeting_id])
         attendee.save
         @new_attendee_ids.push(attendee.id)
     end

     @meeting = Meeting.find(attendee_params[:meeting_id]);
     render :partial => "shared/meeting_attendees"
  end

  def destroy
    @meeting = Meeting.find(@attendee.meeting_id);
    @attendee.destroy
     render :partial => "shared/meeting_attendees"
  end


  private

    def set_attendee
        @attendee = Attendee.find(params[:id])
    end

    def attendee_params
        params.permit(:email, :meeting_id)
    end

end


