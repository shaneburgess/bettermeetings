class ApplicationController < ActionController::Base
  #protect_from_forgery with: :exception
  before_action :require_login

   def require_login
          current_uri = request.env['PATH_INFO'].sub("127.0.0.1:3000",APP_CONFIG['front_door_domain'])
          if !session[:current_user] && current_uri != '/login'
              redirect_to '/login?redirect_to='+current_uri
          end
      end
  
end
