class AgendaItemsController < ApplicationController
  before_action :set_agenda_item, only: [:destroy, :complete]

  def create

    attendee = Attendee.find_or_create_by!(email: agenda_item_params[:attendee_email], meeting_id: agenda_item_params[:meeting_id])

    attendee.save

    logger.debug "Agenda Item attendee id: #{attendee.id}"

    @agenda_item = AgendaItem.create!(agenda_item_text: agenda_item_params[:agenda_item_text], 
                                   meeting_id: agenda_item_params[:meeting_id], 
                                   attendee_id: attendee.id,
                                   time_limit: agenda_item_params[:time_limit]
                                  )

    @agenda_item.save

    @meeting = Meeting.find(@agenda_item.meeting_id);

    render :partial => "shared/meeting_agenda_items"

  end

  def destroy
    meeting_id = @agenda_item.meeting_id

    @agenda_item.destroy

    @meeting = Meeting.find(meeting_id)

    render :partial => "shared/meeting_agenda_items"
  end

  def complete
    meeting_id = @agenda_item.meeting_id

    @agenda_item.done = true

    @agenda_item.save;

    @meeting = Meeting.find(meeting_id)

    render :partial => "shared/meeting_agenda_items"
  end

  private
  def set_agenda_item
    @agenda_item = AgendaItem.find(params[:id])
  end

  def agenda_item_params
    params.require(:agenda_item).permit(:agenda_item_text, :meeting_id, :id, :attendee_email, :time_limit)
  end

end
