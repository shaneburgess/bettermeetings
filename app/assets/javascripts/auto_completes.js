$(document).ready(function() {
    
       //TYPEAHEA


    // constructs the suggestion engine
    var meetingAttendeeBs = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/meetings/'+$("#meeting-id").val()+'/attendees.json?q=%QUERY',
            wildcard: '%QUERY',
            filter: function(data) {

               var array = $.map(data, function(attendee) {
                    return attendee.email;
                });


               return array;
            }

        }
    });


    // constructs the suggestion engine
    var companyAttendeeBs = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/companies/'+$("#company-id").val()+'/attendees?q=%QUERY',
            wildcard: '%QUERY',
        }
    });

    $('.meeting-attendees-typeahead').typeahead({
        hint: true,
        highlight: true,
        minLength: 2,
    }, {
        name: 'meeting-attendees',
        source: meetingAttendeeBs
    });

    $('.company-attendees-typeahead').typeahead({
        hint: true,
        highlight: true,
        minLength: 2,
    }, {
        name: 'company',
        source: companyAttendeeBs
    });

    //TYPEAHEAD
});