$(document).ready(function() {
    
    setInterval(function() {
        $(".flash").removeClass("flash");
    }, 2000)

    var hash = window.location.hash;
    hash && $('ul.nav a[href="' + hash + '"]').tab('show');

    //Set url hash with tab value
    $('.nav-tabs a').click(function(e) {
        window.location.hash = $(this).attr("href");
    });

    $(".datepicker").datepicker({
        format: 'yyyy-mm-dd',
        startDate: '1d'
    });


    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
});