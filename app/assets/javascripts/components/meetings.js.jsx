var Meeting = React.createClass({
   getInitialState: function() {
    return { meeting: {} };
  },
  componentDidMount(){
     $.get("/meetings/1.json").success(function(data){
       this.setState({meeting: data.extract});
     }.bind(this)).fail(function(){
      console.error("There was a problem getting the meeting");
     });
  },
  render: function() {
    return (
        <p>{this.state.meeting.description}</p>
    );
  }
});


ReactDOM.render(<Meeting />,$("#meeting-description")[0])