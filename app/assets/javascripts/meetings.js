//
// MEETINGS.JS
//
//

$(document).ready(function() {

    var agendaItemInterval;
    var activeAgendaItemId = 0;
    var minutesInAnHour = 60;
    var intervalSearchInput = $("#meeting-search-input");

    //Keeping this around so we do can replace it when there is no search
    var meetingListHtml = $("#meeting-list").html();



    intervalSearchInput.keyup(function() {
        if (intervalSearchInput.val().length >= 2) {
            $.get("/meetings?q=" + intervalSearchInput.val()).success(function(data) {
                $("#meeting-list").html(data);
            }).fail(function() {
                alert("There was a problem searching for meetings");
            });
        } else {
            $("#meeting-list").html(meetingListHtml);
        }
    });


    // setInterval(function() {
    //     if(intervalSearchInput.val().length >= 2){
    //         console.log('Searching for '+intervalSearchInput.val());
    //     }
    // }, 500)


    $("#new-meeting-panel-heading").click(function() {
        var panelBody = $("#new-meeting-panel-body")
        panelBody.toggleClass("hide");
        if (panelBody.is(":hidden")) {
            $("#new-meeting-heading-chevron").removeClass("fa-chevron-up");
            $("#new-meeting-heading-chevron").addClass("fa-chevron-down");
        } else {
            $("#new-meeting-heading-chevron").addClass("fa-chevron-up");
            $("#new-meeting-heading-chevron").removeClass("fa-chevron-down");
        }
    });


    //GOALS
    $(document).on("click", ".add-goal-btn", function() {
        if ($("#new_goal").val()) {
            $.post('/goals', {
                goal: {
                    meeting_id: $(this).attr("data-meeting-id"),
                    goal_text: $("#new_goal").val()
                }
            }).done(function(data) {
                $("#goal-container").html(data);
                $("#new_goal").val("");
            }).error(function() {
                alert("There was a problem adding this goal");
            });
        }
    });


    $(document).on("click", ".delete-goal-btn", function() {
        var row = $(this).parent();
        $.ajax({
            method: "DELETE",
            dataType: "text",
            url: '/goals/' + $(this).attr("data-goal-id")
        }).done(function(data) {
            $("#goal-container").html(data);
        }).fail(function() {
            alert("There was a problem deleting this goal");
        });

    });
    //END GOALS

    //AGENDA ITEMS


    $(document).on("click", ".add-agenda-item-btn", function() {

        if ($("#new-agenda-item").val() && $("#new-agenda-item-time-limit").val()) {
            $.post('/agenda_items', {
                agenda_item: {
                    meeting_id: $(this).attr("data-meeting-id"),
                    agenda_item_text: $("#new-agenda-item").val(),
                    attendee_email: $("#new-agenda-item-user-email").val(),
                    time_limit: $("#new-agenda-item-time-limit").val()
                }
            }).done(function(data) {
                $("#agenda-item-container").html(data);
                $("#agenda").find("input").val("");
                 getAttendees();
            }).error(function() {
                alert("There was a problem adding this agenda item");
            });
        }
    });

    $(document).on('click', '.end-timer', function(event) {
        event.preventDefault();

        $.get('/agenda_items/' + activeAgendaItemId + '/complete').success(function(data) {
            $("#agenda-item-container").html(data);
        }).fail(function() {
            alert("There was a problem completing this agenda item");
        });

        $(".start-timer").show();

        if (agendaItemInterval) {
            clearInterval(agendaItemInterval);
        }

        $("#countdown-timer").slideUp(function() {
            $(".agenda-rows").fadeIn();
        });



    });


     $(document).on("click", ".delete-agenda-item", function() {
        var row = $(this).parent();
        $.ajax({
            method: "DELETE",
            dataType: "text",
            url: '/agenda_items/' + $(this).attr("data-agenda-item-id")
        }).done(function(data) {
            $("#agenda-item-container").html(data);
             getAttendees();
        }).fail(function() {
            alert("There was a problem deleting this agenda item");
        });

    });

    $(document).on("click", ".start-timer", function() {
        var timeLeft = $(this).attr("data-time-limit");
        activeAgendaItemId = $(this).attr("data-agenda-item-id");
        console.log(activeAgendaItemId);

        //Hide The start-timer-button
        $(this).hide();

        $("#countdown-timer").hide().removeClass('hide').slideDown();

        //Hide all of the other trs except this one
        $(".agenda-rows").hide();

        $(this).parent().parent().show();





        $("#agenda-item-time-left").text(timeLeft);
        var agendaItemText = $(this).parent().parent().find("td:eq(0)").text();
        $("#agenda-item-timer-text").text(agendaItemText);

        if (agendaItemInterval) {
            clearInterval(agendaItemInterval);
            agendaItemInterval = null;
        } else {
            agendaItemInterval = setInterval(function() {
                var newTimeLeft = parseInt($("#agenda-item-time-left").text()) - 1;
                $("#agenda-item-time-left").text(newTimeLeft);

                if (newTimeLeft > 2) {
                    $("#agenda-item-time-left").addClass("label-success").removeClass("label-danger");
                } else {
                    $("#agenda-item-time-left").removeClass("label-success").addClass("label-danger");
                }

                if (newTimeLeft == 0) {
                    clearInterval(agendaItemInterval);
                    agendaItemInterval = null;
                    $("#time-is-up-modal").modal("show");
                }

            }, 60000);
        }

    });


    //AGENDA ITEMS

    //DELIVERABLES

    $("#add-deliverable-button").click(function(event) {
        var userEmail = $("#new-deliverable-user-email").val();
        var deliverableText = $("#new-deliverable-text").val();
        var dueDate = $("#new-deliverable-due-date").val();
        var meetingId = $('#meeting-id').val();

        $.post('/deliverables', {
            deliverable: {
                user_email: userEmail,
                deliverable_text: deliverableText,
                due_date: dueDate,
                meeting_id: meetingId
            }
        }).success(function(data) {
            $("#deliverable-list").html(data);
            $("#new-deliverable-modal").modal('hide');
            getAttendees();
        }).fail(function() {
            alert("There was a problem adding this deliverable");
        });
    });

    $(document).on("click", ".delete-deliverable-btn", function() {
        var row = $(this).parent();
        $.ajax({
            method: "DELETE",
            dataType: "text",
            url: '/deliverables/' + $(this).attr("data-deliverable-id")
        }).done(function(data) {
            $("#deliverable-list").html(data);
        }).fail(function() {
            alert("There was a problem deleting this deliverable");
        });

    });

    //DELIVERABLES


    //ATTENDEES

    function getAttendees(){
         $.get('/meetings/'+$('#meeting-id').val()+"/attendees.html").success(function(data){
           $("#attendee-container").html(data);
           $("#attendee-tab").addClass("flash");

         }).fail(function(){
             alert("There was a problem updating the attendees list");
         });
    }

    $("#add-attendee-btn").click(function() {

        $.post('/attendees', {
            meeting_id: $('#meeting-id').val(),
            email: $("#new-attendee-email").val()
        }).success(function(data) {
            $("#new-attendee-email").val("");
            $("#attendee-container").html(data);
        }).fail(function() {
            alert("There was a problem adding this attendee");
        });
    });


    $(document).on('click', '.remove-attendee', function(event) {
        event.preventDefault();
        /* Act on the event */
        $.ajax({
                url: '/attendees/' + $(this).attr('data-attendee-id'),
                type: 'DELETE',
                dataType: 'html',
            })
            .success(function(data) {
                $("#attendee-container").html(data);
            })
            .fail(function() {
                alert('There was a problem deleting this attendee');
            });

    });


    //ATTENDEES

    //PARKING LOT ITEMS

    $(document).on('click', '#add-parking-lot-button', function(event) {
        event.preventDefault();
        $.post('/parking_lot_items', {
            text: $("#new-parking-lot-item").val(),
            meeting_id: $('#meeting-id').val()
        }).success(function(data) {
            $("#parking-lot-list").html(data);
            $("#new-parking-lot-item").val("");
        }).fail(function() {
            alert('There was a problem adding this parking lot item');
        });
    });


    $(document).on("click", ".delete-parking-lot-btn", function() {
        var row = $(this).parent();
        $.ajax({
            method: "DELETE",
            dataType: "text",
            url: '/parking_lot_items/' + $(this).attr("data-parking-lot-item-id")
        }).done(function(data) {
            $("#parking-lot-list").html(data);
        }).fail(function() {
            alert("There was a problem deleting this parking lot item");
        });

    });

    //PARKING LOT ITEMS

    //MISC HANDLERS

    $("#send-agenda-button").click(function() {

        $.get('/meetings/'+$('#meeting-id').val()+"/send_goals_and_agenda").success(function(data) {
            $("#agenda-mail-has-been-sent-modal").modal("show");
        }).fail(function() {
            alert("There was a problem sending the agenda");
        });
    });



    $("#send-notes-button").click(function() {

        $.get('/meetings/'+$('#meeting-id').val()+"/send_notes").success(function(data) {
            $("#notes-mail-has-been-sent-modal").modal("show");
        }).fail(function() {
            alert("There was a problem sending the notes");
        });
    });
    //MISC HANDLERS

});