class AttendeeMailer < ApplicationMailer
    default from: APP_CONFIG['default_from_email']

    def goals_and_agenda(meeting,sender)
        @meeting = meeting
        @markdown_rendered = @meeting.markdown_to_html(goals_and_agendas_only: true)
        email_list = @meeting.attendees.map(&:email)
        email_list.push(sender.email)
        mail(to: email_list, subject: "Goals and Agenda for "+@meeting.meeting_name)
    end

    def notes(meeting,sender)
        @meeting = meeting
        @markdown_rendered = @meeting.markdown_to_html(goals_and_agendas_only: false)
        email_list = @meeting.attendees.map(&:email)
        email_list.push(sender.email)
        mail(to: email_list, subject: "Note for "+@meeting.meeting_name)
    end
end
