class Attendee < ApplicationRecord
  belongs_to :meeting
  has_many :deliverables
  has_many :agenda_items
end
