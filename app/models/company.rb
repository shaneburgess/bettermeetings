class Company < ApplicationRecord
    has_many :meetings
    has_many :users
end
