class Meeting < ApplicationRecord
  has_many :goals, :class_name => "Goal"
  has_many :agenda_items, :class_name => "AgendaItem"
  has_many :deliverables, :class_name => "Deliverable"
  has_many :parking_lot_items, :class_name => "ParkingLotItem"
  has_many :attendees, :class_name => "Attendee"


 def markdown_to_html(args)

      markdown_raw = ApplicationController.new.render_to_string('meetings/text', layout: false,locals: { meeting: self, goals_and_agendas_only: args[:goals_and_agendas_only]  })

      renderer = Redcarpet::Render::HTML.new(no_links: true, hard_wrap: true)

      markdown = Redcarpet::Markdown.new(renderer, extensions = {strikethrough: true})

      return markdown.render(markdown_raw)
  end
end
