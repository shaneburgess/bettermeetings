class AgendaItem < ApplicationRecord
  before_create :set_order
  belongs_to :meeting
  belongs_to :attendee

private
  def set_order
    max_order = AgendaItem.maximum(:order)
    
    self.order = 1
    
    if max_order
        self.order = max_order+1
    end

  end
end
