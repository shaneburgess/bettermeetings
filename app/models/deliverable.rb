class Deliverable < ApplicationRecord
    belongs_to :attendee, :class_name => "Attendee"
    belongs_to :meeting
end
