json.extract! setting, :id, :company_name, :average_salary, :number_of_employees, :created_at, :updated_at
json.url setting_url(setting, format: :json)
