json.extract meeting, :id, :meeting_name, :description, :start_time, :end_time, :organizer, :time_keeper, :note_taker, :actual_end_time, :created_at, :updated_at
json.url meeting_url(meeting, format: :json)
