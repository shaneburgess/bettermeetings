json.extract! comment, :id, :meeting_id_id, :user_id_id, :comment, :created_at, :updated_at
json.url comment_url(comment, format: :json)
