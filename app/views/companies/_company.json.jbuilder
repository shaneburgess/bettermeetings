json.extract! company, :id, :name, :address, :address_ext, :city, :state, :zip, :created_at, :updated_at
json.url company_url(company, format: :json)
