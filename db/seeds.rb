# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

#Clear the db in an orderly and safe way
Attendee.delete_all()

ParkingLotItem.delete_all()

AgendaItem.delete_all()

Deliverable.delete_all()

Goal.delete_all()

Meeting.delete_all()

User.delete_all()

Company.delete_all()
