# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170316234606) do

  create_table "agenda_items", force: :cascade do |t|
    t.integer  "meeting_id"
    t.integer  "attendee_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "time_limit"
    t.string   "agenda_item_text"
    t.boolean  "done"
    t.integer  "order"
    t.index ["attendee_id"], name: "index_agenda_items_on_attendee_id"
    t.index ["meeting_id"], name: "index_agenda_items_on_meeting_id"
  end

  create_table "attendees", force: :cascade do |t|
    t.integer  "meeting_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "email"
    t.index ["meeting_id"], name: "index_attendees_on_meeting_id"
  end

  create_table "companies", force: :cascade do |t|
    t.string   "name"
    t.string   "address"
    t.string   "address_ext"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.integer  "average_salary"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "unique_email"
  end

  create_table "deliverables", force: :cascade do |t|
    t.integer  "meeting_id"
    t.datetime "due_date"
    t.integer  "attendee_id"
    t.boolean  "complete"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "deliverable_text"
    t.index ["attendee_id"], name: "index_deliverables_on_attendee_id"
    t.index ["meeting_id"], name: "index_deliverables_on_meeting_id"
  end

  create_table "goals", force: :cascade do |t|
    t.integer  "meeting_id"
    t.boolean  "completed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "goal_text"
    t.index ["meeting_id"], name: "index_goals_on_meeting_id"
  end

  create_table "meetings", force: :cascade do |t|
    t.string   "meeting_name"
    t.text     "description"
    t.datetime "start_time"
    t.datetime "end_time"
    t.integer  "organizer"
    t.integer  "time_keeper"
    t.integer  "note_taker"
    t.datetime "actual_end_time"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "company_id",      null: false
    t.datetime "date_time"
    t.date     "date"
    t.index ["company_id"], name: "index_meetings_on_company_id"
  end

  create_table "parking_lot_items", force: :cascade do |t|
    t.integer  "meeting_id"
    t.string   "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["meeting_id"], name: "index_parking_lot_items_on_meeting_id"
  end

  create_table "settings", force: :cascade do |t|
    t.string   "company_name"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "company_id"
    t.index ["company_id"], name: "index_settings_on_company_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "password_digest"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "user_type"
    t.integer  "company_id",      null: false
    t.index ["company_id"], name: "index_users_on_company_id"
  end

end
