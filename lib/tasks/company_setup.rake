
namespace :company_setup do
      desc "Add the company"

      @company

      task :add_company => :environment do
        @company = Company.find_or_create_by( :name => APP_CONFIG['company_name'])
      end

      desc "Add the admin user"
      task :add_admin_user => :environment do
        user = @company.users.find_or_create_by(email: APP_CONFIG['admin_email'], first_name: APP_CONFIG['admin_first_name'], last_name: APP_CONFIG['admin_last_name'], user_type: 'Admin')
        user.password = "mypass"
        user.save
      end

      desc "Run all company setup tasks"
      task :all => [:add_company, :add_admin_user]
end
