Rails.application.routes.draw do

  get 'attendee/create'

  get 'attendee/delete'

  get '/companies/:id/attendees', to: 'companies#attendees'

  resources :companies
  get 'index/home'

  resources :comments
  resources :meetings
  resources :settings
  resources :users
  resources :goals
  resources :agenda_items
  resources :deliverables

  get '/agenda_items/:id/complete', to: 'agenda_items#complete'

  get '/meetings/:id/send_goals_and_agenda', to:"meetings#send_goals_and_agenda"

  get '/meetings/:id/send_notes', to:"meetings#send_notes"

  post '/parking_lot_items', to: 'parking_lot_items#create'

  delete '/parking_lot_items/:id', to: 'parking_lot_items#destroy'

  post '/attendees', to:'attendee#create'

  delete '/attendees/:id', to:'attendee#destroy'

  root 'meetings#index'
  
  get '/login', to: 'auth#login', as: 'auth_login_url'
  get '/logout', to: 'auth#logout', as: 'auth_logout_url'
  post '/login', to: 'auth#process_login'

  get '/meetings/:id/text', to: 'meetings#text'

  get '/meetings/:id/md', to: 'meetings#markdown'

  get '/meetings/:id/attendees', to: 'meetings#get_meeting_attendees'
    
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
