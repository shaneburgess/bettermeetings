working_directory "/vagrant/rails/BetterMeetings"
pid "/vagrant/rails/BetterMeetings/tmp/unicorn.pid"
stderr_path "/vagrant/rails/BetterMeetings/log/unicorn.log"
stdout_path "/vagrant/rails/BetterMeetings/log/unicorn.log"
listen 3000
worker_processes 2
timeout 60