#Set SES as the mailer

Rails.application.config.action_mailer.delivery_method = :aws_sdk


Aws.config[:credentials] = Aws::Credentials.new(APP_CONFIG['aws']['ses_access_key'], APP_CONFIG['aws']['ses_secret'])

Aws.config[:region] = 'us-east-1'

Aws.config[:ses] = {:region => 'us-east-1'}


