require 'test_helper'

class AttendeeControllerTest < ActionDispatch::IntegrationTest
  test "should get create" do
    get attendee_create_url
    assert_response :success
  end

  test "should get delete" do
    get attendee_delete_url
    assert_response :success
  end

  

end
